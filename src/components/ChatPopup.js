import {useRef, useState, useContext} from 'react'
import "../css/ChatPopup.css"
import Emojis from "./Emojis.js"
import useClickOutside from "../hooks/useClickOutside"
import { PlaylistContext } from './Room'

const ChatPopup = ({name, content, setShowPopup, setMessage}) => {
    /* const [isClosing, setIsClosing] = useState(false) */

    const ref = useRef()
    const { users } = useContext(PlaylistContext)

    useClickOutside(ref, () => {
        /* setIsClosing(true) */
        setShowPopup(false)
    })

    return (
        <div className="chat-popup" ref={ref}>
            {
                content === "users" ?
                    <RoomUsers name={name} users={users} />
                :   <EmojiPicker setMessage={setMessage} />
            }
        </div>
    )
}

const EmojiPicker = ({setMessage}) => {
    const handleClickedEmoji = (emojiCode) => {
        setMessage(prev => {
            return prev += ` ${emojiCode}`
        })
    }

    return (
        <div className="emoji-picker">
            {
                Emojis.map((item, index) => {
                    return (
                        <div 
                            key={index} 
                            className="single-emoji"
                            onClick={() => handleClickedEmoji(item)}>
                            {item}
                        </div>
                    )
                })
            }
        </div>
    )
}

const RoomUsers = ({users, name}) => {
    return (
        <div className="room-users">
            {
                users.map((user, index) => {
                    return (
                        <div key={index} className="room-username-holder">
                            <span className="room-username">{user.name}{name === user.name && <span className="your-name">(you)</span>}</span>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default ChatPopup