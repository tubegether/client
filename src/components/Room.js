import { useEffect, useState, createContext } from "react"
import StaticContent from "./StaticContent"
import ScrollView from "./ScrollView"
import TopBar from "./TopBar"
import "../css/Room.css"
import { useParams } from "react-router-dom"
import UsernamePopup from "./UsernamePopup"
import { socket } from "../service/socket"
import useWindowSize from "../hooks/useWindowSize"
import MobileSearch from "./MobileSearch"

export const PlaylistContext = createContext()

const Room = () => {
    const {roomID} = useParams()
    const [name, setName] = useState(localStorage.getItem("username") ? localStorage.getItem("username") : "")
    const [showPopup, setShowPopup] = useState(true)
    const [searchString, setSearchString] = useState("")
    const [playlist, setPlaylist] = useState([])
    const [nowPlaying, setNowPlaying] = useState({})
    const [users, setUsers] = useState([])
    const [mobileSearchOpen, setMobileSearchOpen] = useState(false)
    const windowSize = useWindowSize()

    useEffect(() => {
        if(!socket) return

        socket.on("add-to-queue", videoData => {
            setPlaylist(prev => {
                sessionStorage.setItem("playlist", JSON.stringify([...prev, videoData]))
                return [...prev, videoData]
            })
        })

        socket.on("play-now-from-playlist", data => {
            setNowPlaying(data.videoData)
            sessionStorage.setItem("now-playing", JSON.stringify(data.videoData))
            setPlaylist(prev => {
                const newPlaylist = prev.filter((_, i) => i != data.index)
                sessionStorage.setItem("playlist", JSON.stringify(newPlaylist))
                return newPlaylist
            })
        })

        socket.on("delete-from-playlist", index => {
            setPlaylist(prev => {
                const newPlaylist = prev.filter((_, i) => i != index)
                sessionStorage.setItem("playlist", JSON.stringify(newPlaylist))
                return newPlaylist
            })
        })

        socket.on("give-username", (data) => {
            socket.emit("username", {name: name, destination_id: data.id, id: socket.id})
        })

        socket.on("received-username", data => {
            setUsers(prev => {
                if(prev.find(el => el.id === data.id) == undefined) {
                    return [...prev, {name: data.name, id: data.id}]
                } else {
                    return [...prev]
                }
            })
        })

        socket.on("new-user", data => {
            //sessionStorage.setItem("socket_id", data.id)
            setUsers(prev => {
                return [...prev, {name: data.name, id: data.id}]
            })
        })

        socket.on("give-room-data", destination_id => {
            const playingNow = JSON.parse(sessionStorage.getItem("now-playing"))
            const playlistToSend = JSON.parse(sessionStorage.getItem("playlist"))
            socket.emit("room-data", {destination_id: destination_id, nowPlaying: playingNow, playlist: playlistToSend})
        })

        socket.on("received-room-data", data => {
            if(data.nowPlaying != null) {
                setNowPlaying(data.nowPlaying)
                sessionStorage.setItem("now-playing", JSON.stringify(data.nowPlaying))
            }

            if(data.playlist != null) {
                setPlaylist(data.playlist)
                sessionStorage.setItem("playlist", JSON.stringify(data.playlist))
            }
        })

        socket.on("user-disconnected", user_id => {
            setUsers(prev => {
                return prev.filter((user,index) => user_id != user.id)
            })
        })


        return () => {
            socket.off("add-to-queue")
            socket.off("play-now-from-playlist")
            socket.off("delete-from-playlist")
            socket.off("give-username")
            socket.off("recieved-username")
            socket.off("new-user")
            socket.off("connect")
            socket.off("disconnect")
            socket.off("user-disconnected")
            //socket.off("give-progress")
        }
    }, [socket, name])

    return (
        <>
        <div>
            {
                windowSize.width >= 768 && 
                    <TopBar 
                    searchString={searchString} 
                    setSearchString={setSearchString} 
                    setPlaylist={setPlaylist}
                    setNowPlaying={setNowPlaying} 
                    socket={socket}
                    roomID={roomID}/>
            }
            { (mobileSearchOpen && windowSize.width < 768) && 
                <MobileSearch 
                    searchString={searchString} 
                    setSearchString={setSearchString} 
                    setPlaylist={setPlaylist}
                    setNowPlaying={setNowPlaying} 
                    socket={socket}
                    roomID={roomID}
                    setMobileSearchOpen={setMobileSearchOpen}
                />
            }
            <div className="container room-container">
                <PlaylistContext.Provider 
                    value={{
                        playlist: playlist, 
                        nowPlaying: nowPlaying,
                        setNowPlaying: setNowPlaying,
                        setPlaylist: setPlaylist,
                        socket: socket,
                        roomID: roomID,
                        users: users,
                        setMobileSearchOpen: setMobileSearchOpen
                    }}
                    >
                    <ScrollView socket={socket} name={name} roomID={roomID}/>
                    {
                        windowSize.width >= 768 && 
                            <StaticContent socket={socket} name={name} roomID={roomID}/>
                    }
                </PlaylistContext.Provider>
            </div>
        </div>
        {
            showPopup && <UsernamePopup roomID={roomID} socket={socket} setName={setName} setShowPopup={setShowPopup} setPlaylist={setPlaylist} setNowPlaying={setNowPlaying}/>
        }
        </>
    )
}

export default Room