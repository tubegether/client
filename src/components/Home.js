import { useState, useEffect } from "react"
import { useNavigate } from "react-router-dom"
import { ReactComponent as GitlabIcon } from "../svg/gitlab.svg"
import { ReactComponent as WebsitesIcon } from "../svg/websites.svg"
import "../css/Home.css"

const ROOM_ID_LENGTH = 6

const Home = () => {
    const navigate = useNavigate()

    const makeRoomID = (length) => {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        
        for(let i = 0; i < length; i++) {
           result += characters.charAt(Math.floor(Math.random() * characters.length));
        }
        return result;
    }

    const createRoom = () => {
        const roomID = makeRoomID(ROOM_ID_LENGTH)
        navigate(`/room/${roomID}`)
    }

    return (
        <div className="home">
            <div className="home-container">
                <div className="home-logo">
                    <span className="logo-color">TUBE</span>
                    <span>GETHER</span>
                </div>
                <p className="home-description">
                    Just click "create room", share link with your friends and that's it. TubeGether for now supports only YouTube and Twitch videos or live streams but in the future there for sure will be more websites supported.
                </p>
                <div className="home-buttons">
                    <div className="links">
                        <a href="https://gitlab.com/tubegether" target="_blank" className="gitlab-link">
                            <GitlabIcon className="svg-icon" />
                            GitLab
                        </a>
                    </div>
                    <button 
                        className="create-room-btn"
                        onClick={createRoom}>
                        create room
                    </button>
                </div>
            </div>
        </div>
    )
}

export default Home