const STYLES = {
    width: "100%",
    height: "100%"
}

const TwitchChat = ({url}) => {
    const user = new URL(url).pathname.replace("/", "")

    return (
        <div className="twitch-chat" style={STYLES}>
            <iframe id="twitch-chat-embed"
                    src={`https://www.twitch.tv/embed/${user}/chat?parent=tubegether.onrender.com`}
                    height="100%"
                    width="100%"
                    style={{border: "none"}}>
            </iframe>
        </div>
    )
}

export default TwitchChat
