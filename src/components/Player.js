import { useEffect, useState, useRef, useContext } from "react"
import "../css/Player.css"
import ReactPlayer from 'react-player'
import screenfull from "screenfull"
import {ReactComponent as PlayIcon} from "../svg/play.svg"
import {ReactComponent as PauseIcon} from "../svg/pause.svg"
import {ReactComponent as VolumeIcon} from "../svg/volume.svg"
import {ReactComponent as FullscreenIcon} from "../svg/fullscreen.svg"
import {ReactComponent as VideoIcon} from "../svg/video.svg"
import { PlaylistContext } from "./Room"
import { browserName, isMobile } from "react-device-detect"

const Player = () => {
    const [playing, setPlaying] = useState(true)
    const [volume, setVolume] = useState(isMobile ? 0.7 :  0.2)
    const [progress, setProgress] = useState(0)
    const [duration, setDuration] = useState(0)
    const [isSeeking, setIsSeeking] = useState(false)
    const [isFullscreen, setIsFullscreen] = useState(false)
    const [progressRequestSend, setProgressRequestSend] = useState(false)
    const playerRef = useRef()
    const wrapperRef = useRef()
    const { playlist, nowPlaying, setNowPlaying, setPlaylist, socket, roomID } = useContext(PlaylistContext)

    const handlePlay = () => {
        socket.emit("resume-video", roomID)
        setPlaying(true)
    }
    const handlePause = () => {
        socket.emit("pause-video", roomID)
        setPlaying(false)
    }
    const handleVideoReady = () => {
       if(nowPlaying.website != "twitch" && !progressRequestSend) {
            socket.emit("give-progress", {id: socket.id, room: roomID})
            setProgressRequestSend(true)
       }
    }
    const handlePlayPause = () => setPlaying(!playing)
    const handleDuration = duration => setDuration(duration)
    const handleProgress = ({playedSeconds}) => {
        if(!isSeeking) {
            setProgress(playedSeconds)
            sessionStorage.setItem("progress", playedSeconds)
        }
    }

    const handleFullscreen = () => {
        screenfull.toggle(wrapperRef.current)
        
        isFullscreen ? setIsFullscreen(false) : setIsFullscreen(true)
    }

    const handleVideoEnd = () => {
        if(playlist.length > 0) {
            setNowPlaying({
                url: playlist[0].link,
                website: playlist[0].data.website,
                type: playlist[0].data.type
            })

            sessionStorage.setItem("now-playing", JSON.stringify({
                url: playlist[0].link,
                website: playlist[0].data.website,
                type: playlist[0].data.type
            }))

            setPlaylist(prev => {
                const newPlaylist = prev.filter((_, index) => index != 0)
                sessionStorage.setItem("playlist", JSON.stringify(newPlaylist))
                return newPlaylist
            })
        }
    }

    //SOCKETS//
    useEffect(() => {
        if(!socket) return

        socket.on("play-now", data => {
            setNowPlaying(data)
            sessionStorage.setItem("now-playing",JSON.stringify(data))
        })

        socket.on("pause-video", () => setPlaying(false))
        socket.on("resume-video", () => setPlaying(true))
        socket.on("received-progress", prog => {
            if(prog - progress >= 5) {
                setProgress(prog + 1)
                playerRef.current.seekTo(prog + 1, "seconds")
                sessionStorage.setItem("progress", prog + 1)
            }
        })

        socket.on("give-progress", destination_id => {
            socket.emit("progress", {id: destination_id, progress: parseInt(playerRef.current.getCurrentTime())})
        })

        return () => {
            socket.off("play-now")
            socket.off("pause-video")
            socket.off("resume-video")
            socket.off("received-progress")
        }
    }, [socket])

    return (
        <div className="video-player" ref={wrapperRef}>
            <div className="player-wrapper">
                {
                    Object.entries(nowPlaying).length === 0 && 
                        <div className="player-placeholder">
                            <VideoIcon className="svg-icon player-placeholder-icon" />
                            <span className="player-placeholder-message">
                                Search or paste video link to start playing.
                            </span>
                        </div>
                }
                <ReactPlayer 
                    className="react-player"
                    url={nowPlaying?.url}
                    width="100%"
                    height="100%"
                    ref={playerRef}
                    playing={playing}
                    volume={volume}
                    onPause={handlePause}
                    onPlay={handlePlay}
                    onEnded={handleVideoEnd}
                    controls={false}
                    onProgress={handleProgress}
                    onDuration={handleDuration}
                    onReady={handleVideoReady}
                    />
            </div>
            <div className={`player-controls ${isFullscreen && 'player-controls-fullscreen'} ${nowPlaying.type === "live" && "player-controls-live"}`}>
                <div className="player-controls-left">
                    <PlayPauseButton 
                        handlePlayPause={handlePlayPause} 
                        playing={playing} 
                        />
                    <VolumeSlider volume={volume} setVolume={setVolume}/>
                    {
                        nowPlaying.type === "live" &&
                            <div className="its-live">
                                <span>LIVE</span>
                            </div>
                    }
                </div>
                {
                    nowPlaying.type !== "live" && 
                        <ProgressSlider 
                        progress={progress} 
                        duration={duration}
                        setIsSeeking={setIsSeeking}
                        setProgress={setProgress}
                        playerRef={playerRef}
                        socket={{socket: socket, room: roomID}}/>
                }
                <div 
                    className="fullscreen-button"
                    onClick={handleFullscreen}
                    >
                    <FullscreenIcon className="fullscreen-button-icon player-control-icon" />
                </div>
            </div>
        </div>
    )
}

const PlayPauseButton = ({playing, handlePlayPause}) => {
    return (
        <div className="play-pause" onClick={handlePlayPause}>
            {
                playing ? 
                    <PauseIcon className="play-pause-icon player-control-icon" /> 
                : 
                    <PlayIcon  className="play-pause-icon player-control-icon"/>
            }
        </div>
    )
}

const VolumeSlider = ({volume, setVolume}) => {
    const [showVolumeRange, setShowVolumeRange] = useState(false)
    const volumeRef = useRef()

    const handleVolumeChange = e => {
        setVolume(parseFloat(e.target.value))
    }

    //Updating progress bar for chromium browsers
    useEffect(() => {
        if(showVolumeRange && browserName !== "Firefox")
            updateProgress(volume, volumeRef)
    }, [volume, showVolumeRange])

    return (
        <div 
            className="volume-slider"
            onMouseOver={() => !isMobile && setShowVolumeRange(true)}
            onMouseLeave={() => !isMobile && setShowVolumeRange(false)}
            onClick={() => isMobile && setShowVolumeRange(!showVolumeRange)}
            >
            <div className="volume-slider-icon-container">
                <VolumeIcon className="volume-slider-icon player-control-icon"/>
            </div>
            <div className="volume-range">
                {
                    showVolumeRange && 
                        <input 
                        type="range" 
                        min={0} 
                        max={1}
                        step="0.001"
                        value={volume} 
                        onChange={handleVolumeChange}
                        className={`volume-range-input ${browserName !== "Firefox" && "not-firefox"}`}
                        ref={volumeRef}/>
                }
            </div>
        </div>
    )
}

const ProgressSlider = ({progress, duration, setIsSeeking, setProgress, playerRef, socket}) => {
    const progressSliderRef = useRef()


    const handleMouseUp = e => {
        setIsSeeking(false)
        playerRef.current.seekTo(parseInt(e.target.value), "seconds")
        socket.socket.emit("seek-to", {seconds: parseInt(e.target.value), room: socket.room})
    }

    useEffect(() => {
        if(!socket.socket) return
        
        socket.socket.on("seek-to", sec => {
            playerRef.current.seekTo(sec, "seconds")
            setProgress(sec)
        })

        return () => {
            socket.socket.off("seek-to")
        }
    }, [socket.socket])

    //Updating progress bar for chromium browsers
    useEffect(() => {
        if(browserName !== "Firefox")
            updateProgress(progress, progressSliderRef)
    }, [progress])
    
    return (
        <div className="progress-slider">
            <div className="progress-slider-time">
                <span>
                    <Timer progress={progress}/> / <Timer progress={duration} />
                </span>
            </div>
            <div className="progress-slider-container">
                <input 
                    type="range" 
                    className={`progress-range ${browserName !== "Firefox" && "not-firefox"}`}
                    min={0}
                    max={duration}
                    step="any" 
                    value={progress}
                    onMouseDown={() => setIsSeeking(true)}
                    onChange={(e) => setProgress(parseInt(e.target.value))}
                    onMouseUp={handleMouseUp}
                    onTouchStart={() => setIsSeeking(true)}
                    onTouchEnd={handleMouseUp}
                    ref={progressSliderRef}
                    />
            </div>
        </div>
    )
}

const Timer = ({progress}) => {
    if(!progress) return <span>0:00</span>

    const seconds = parseInt(progress % 60)
    const minutes = parseInt((progress / 60) % 60) 
    const hours = parseInt(progress / 3600)

    return (
        <span>
            {hours > 0 && `${hours}:`}
            {(hours > 0 && minutes  < 10) && "0"}{minutes}:{seconds % 60 < 10 && "0"}{seconds}
        </span>
    )

}

const updateProgress = (progress, inputRef) => {
    const percentage = (progress - inputRef.current.min) / (inputRef.current.max - inputRef.current.min) * 100;
    inputRef.current.style.backgroundImage = `linear-gradient(90deg, var(--theme-color) ${percentage}%, transparent ${percentage}%)`;
}

export default Player