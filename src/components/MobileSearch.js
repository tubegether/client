import ReactDOM from "react-dom";
import TopBar from "./TopBar";

const STYLES = {
    width: "100%",
    height: "100vh",
    position: "fixed",
    left: "0",
    top: "0",
    backgroundColor: "rgba(0,0,0,0.9)",
    zIndex: "10000"
}

const MobileSearch = ({searchString, setSearchString, setPlaylist, setNowPlaying, socket, roomID, setMobileSearchOpen}) => {
    return ReactDOM.createPortal(
        <div className="mobile-search" style={STYLES}>
            <TopBar 
                searchString={searchString} 
                setSearchString={setSearchString} 
                setPlaylist={setPlaylist}
                setNowPlaying={setNowPlaying} 
                socket={socket}
                roomID={roomID}
                setMobileSearchOpen={setMobileSearchOpen}/>
        </div>
        , document.getElementById("mobile-search")
    )
}

export default MobileSearch