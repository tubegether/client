import { useContext, useEffect } from 'react'
import { PlaylistContext } from './Room'
import "../css/Playlist.css"
import {ReactComponent as TrashIcon} from "../svg/trash.svg"
import {ReactComponent as TwitchIcon} from "../svg/twitch.svg"
import {ReactComponent as YoutubeIcon} from "../svg/youtube.svg"
import {ReactComponent as QueueIcon} from "../svg/queue.svg"

const Playlist = () => {
    const { playlist, nowPlaying, setNowPlaying, setPlaylist, socket, roomID } = useContext(PlaylistContext)

    const handlePlayItemNow = (index) => {
        const data = {
            url: playlist[index].link,
            website: playlist[index].data.website,
            type: playlist[index].data.type
        }
        socket.emit("play-now-from-playlist", {data: data,index: index, room: roomID})
        setNowPlaying(data)
        sessionStorage.setItem("now-playing", JSON.stringify(data))
        setPlaylist(prev => {
            const newPlaylist = prev.filter((_, i) => i != index)
            sessionStorage.setItem("playlist", JSON.stringify(newPlaylist))
            return newPlaylist
        })
    }

    const handleDeleteItem = (index) => {
        socket.emit("delete-from-playlist", {index: index, room: roomID})
        setPlaylist(prev => {
            const newPlaylist = prev.filter((_, i) => i != index)
            sessionStorage.setItem("playlist", JSON.stringify(newPlaylist))
            return newPlaylist
        })
    }

    
    return (
        <>
        <div className="playlist-top">
            <h1 className="playlist-title">Playlist</h1>
        </div>
        <div className="playlist">
            {
                playlist.length === 0 &&
                    <div className="empty-playlist">
                        <QueueIcon className="svg-icon empty-playlist-icon" />
                    </div>
                
            }
            {
                playlist.map((item, index) => {
                    return (
                        <div 
                            key={index} 
                            className={`playlist-item ${index == 0 && "first-item"}`}>
                            <div 
                                className="playlist-item-l">
                                <div className="playlist-item-thumbnail">
                                    {/* <div className="video-website-identifier">
                                    {
                                        item?.data.website === 'youtube' ? 
                                            <YoutubeIcon className="svg-icon youtube-identifier" /> :
                                            <TwitchIcon className="svg-icon twitch-identifier" /> 
                                    }
                                    </div> */}
                                    <img 
                                        src={item?.data?.thumbnail} 
                                        width="100%" />
                                </div>
                                <div className="playlist-item-details">
                                    <h2 
                                        className="playlist-item-title"
                                        title={item?.data?.title}
                                        onClick={() => handlePlayItemNow(index)}
                                        >
                                        {item?.data?.title}
                                    </h2>
                                    <div className="playlist-item-author-type">
                                        <h4 className="playlist-item-author">{item?.data?.channel}</h4>
                                        {
                                            item.data.type === "live" && 
                                                <div className="its-live playlist-item-live">
                                                    <span>LIVE</span>
                                                </div>
                                        }
                                    </div>
                                </div>
                            </div>
                            <div 
                            className="playlist-item-button"
                            onClick={() => handleDeleteItem(index)}>
                                <TrashIcon className="svg-icon playlist-item-icon" />
                            </div>
                            <div className="playlist-item-number">
                                <span>{index + 1}</span>
                            </div>
                        </div>
                    )
                })
            }
        </div>
        </>
    )
}

export default Playlist