import { useEffect, useState } from "react"
import ReactDOM from "react-dom"
import "../css/UsernamePopup.css"

const UsernamePopup = ({roomID, socket, setName, setShowPopup, setPlaylist, setNowPlaying}) => {
    const [username, setUsername] = useState(localStorage.getItem("username") ? localStorage.getItem("username") : "")

    const generateRandomName = () => {
        let result = ""
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        
        for(let i = 0; i < 8; i++) {
            result += characters.charAt(Math.floor(Math.random() * characters.length))
        }

        return result
    }

    useEffect(() => {
        if(username == "") {
            setUsername(generateRandomName())
        }
    }, [])

    const applyUsername = () => {
        setName(username)
        localStorage.setItem("username", username)
        sessionStorage.setItem("socket_id", socket.id)
        socket.emit("join-room", roomID)
        socket.emit("user-connected", {name: username, room: roomID, id: socket.id})

        if(sessionStorage["playlist"] || sessionStorage["now-playing"]) {
            if(sessionStorage["playlist"])
                setPlaylist(JSON.parse(sessionStorage["playlist"]))

            if(sessionStorage["now-playing"])
                setNowPlaying(JSON.parse(sessionStorage["now-playing"]))
        } else {
            socket.emit("give-room-data", {id: socket.id, room: roomID})
        }

        socket.emit("give-usernames", {id: socket.id, room: roomID})
        setShowPopup(false)
    }


    return ReactDOM.createPortal(
        <div className="username-popup-container">
            <div className="username-popup">
                <label className="username-label">
                    Name
                </label>
                <input 
                    type="text" 
                    className="username-input" 
                    placeholder="Enter Name" 
                    value={username} 
                    onChange={(v) => setUsername(v.target.value)}
                />
                <button 
                    className="username-button"
                    onClick={applyUsername}
                >
                    Join
                </button>
            </div>
        </div>
        ,document.getElementById('room-popup')
    )
}

export default UsernamePopup