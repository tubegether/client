import "../css/ScrollView.css"
import Player from "./Player"
import useWindowSize from "../hooks/useWindowSize"
import StaticContent from "./StaticContent"

const ScrollView = ({socket, name, roomID}) => {
    const windowSize = useWindowSize()

    return (
        <div className="scroll-view">
            <Player />
            {
                windowSize.width < 768 &&
                <StaticContent socket={socket} name={name} roomID={roomID} />
            }
        </div>
    )
}

export default ScrollView