import "../css/StaticContent.css"
import Chat from "./Chat"
import Playlist from "./Playlist"
import { ReactComponent as ChatIcon } from "../svg/chat.svg"
import { ReactComponent as QueueIcon } from "../svg/queue.svg"
import { ReactComponent as TwitchIcon } from "../svg/twitch.svg"
import { ReactComponent as YoutubeIcon } from "../svg/youtube.svg"
import { ReactComponent as SearchIcon } from "../svg/search.svg"
import { useState, useContext, useEffect } from "react"
import TwitchChat from "./TwitchChat"
import { PlaylistContext } from "./Room"
import YoutubeChat from "./YoutubeChat"
import useWindowSize from "../hooks/useWindowSize"

const StaticContent = ({socket, name, roomID}) => {
    const [content, setContent] = useState('chat')
    const windowSize = useWindowSize()
    
    const { nowPlaying, setMobileSearchOpen } = useContext(PlaylistContext)

    useEffect(() => {
        if((content === "youtube-chat" || content === "twitch-chat") && nowPlaying.type != "live")
            setContent('chat')

        if((content === "youtube-chat" || content === "twitch-chat") && nowPlaying.type == "live") {
            if(content === "youtube-chat" && nowPlaying.website !== "youtube") {
                setContent('chat')
            } else if(content === "twitch-chat" && nowPlaying.website !== "twitch") {
                setContent('chat')
            }
        }
    }, [nowPlaying])

    return (
        <div className="static-content-container">
            <div className="static-content">
                <div className="content-nav">
                    <div 
                        className="content-nav-btn"
                        title="chat"
                        onClick={() => setContent('chat')}>
                        <ChatIcon className={`svg-icon content-nav-icon ${content === "chat" && "active-btn"}`}/>
                    </div>
                    <div 
                        className="content-nav-btn" 
                        title="playlist"
                        onClick={() => setContent('playlist')}>
                        <QueueIcon className={`svg-icon content-nav-icon playlist-icon ${content === "playlist" && "active-btn"}`}/>
                    </div>
                    {
                        nowPlaying.website === "twitch" && 
                        <div 
                            className="content-nav-btn" 
                            title="Chat from twitch"
                            onClick={() => setContent('twitch-chat')}>
                            <TwitchIcon className={`svg-icon content-nav-icon twitch-icon ${content === "twitch-chat" && "active-btn"}`}/>
                        </div>
                    }
                    {
                        (nowPlaying.website === "youtube" && nowPlaying.type === "live") &&
                        <div 
                            className="content-nav-btn" 
                            title="Chat from youtube"
                            onClick={() => setContent('youtube-chat')}>
                            <YoutubeIcon className={`svg-icon content-nav-icon youtube-icon ${content === "youtube-chat" && "active-btn"}`}/>
                        </div> 
                    }
                    {
                        windowSize.width < 768 && 
                        <div 
                            className="content-nav-btn" 
                            title="Search"
                            onClick={() => setMobileSearchOpen(true)}>
                            <SearchIcon className={`svg-icon content-nav-icon search-icon`}/>
                        </div> 
                    }
                </div>
                {
                    content === "chat" && <Chat  socket={socket} name={name} roomID={roomID} />
                }
                {
                    content === "playlist" && <Playlist /> 
                }
                {
                    content === "twitch-chat" && <TwitchChat url={nowPlaying.url}/>
                }
                {
                    content === "youtube-chat" && <YoutubeChat url={nowPlaying.url} />
                }
            </div>
        </div>
    )
}

export default StaticContent