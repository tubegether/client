import {ReactComponent as DotsIcon} from "../svg/dots.svg"
import {ReactComponent as UserIcon} from "../svg/user.svg"
import {ReactComponent as EmoteIcon} from "../svg/emote.svg"
import {useEffect, useState} from 'react'
import "../css/Chat.css"
import ChatPopup from "./ChatPopup"

const Chat = ({socket, name, roomID}) => {
    const [message, setMessage] = useState("")
    const [messages, setMessages] = useState(sessionStorage.getItem("messages") ? JSON.parse(sessionStorage.getItem("messages")) : [])
    const [showPopup, setShowPopup] = useState(false)
    const [popupContent, setPopupContent] = useState("")

    const handlePopup = (content) => {
        setPopupContent(content)
        setShowPopup(true)
    }

    const sendMessage = (e) => {
        e.preventDefault()
        socket.emit("new-message", {name: name, roomID: roomID, message: message})
        setMessage("")
    }

    useEffect(() => {
        if(!socket) return

        socket.on("msg", (data) => {
            setMessages(prevValue => {
                sessionStorage.setItem("messages", JSON.stringify([...prevValue, data]))
                return [...prevValue, data]
            })
        })

        return () => {
            socket.off("msg")
        }
    }, [socket])

    return (
        <div className="chat">
            <div className="chat-top-bar">
                <h1 className="chat-title">Chat</h1>
            </div>
            <div className="chat-messages-container">
                {
                    messages.map((item, index) => {
                        return <Message key={index} name={item.name} message={item.message} />
                    })
                }
            </div>
            <form className="chat-send-container">
                {
                    showPopup && 
                        <ChatPopup name={name} content={popupContent} setShowPopup={setShowPopup} setMessage={setMessage}/>
                }
                <div className="chat-input-holder">
                    <input 
                        type="text" 
                        className="chat-input" 
                        placeholder="Chat"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                    />
                </div>
                <div className="chat-button-holder">
                    <div className="chat-icons-holder">
                        <div 
                            className="single-icon users-icon"
                            onClick={() => handlePopup("users")}>
                            <UserIcon className="svg-icon chat-icon"/>
                        </div>
                        <div 
                            className="single-icon emotes-icon"
                            onClick={() => handlePopup("emojis")}>
                            <EmoteIcon className="svg-icon chat-icon"/>
                        </div>
                    </div>
                    <button 
                        type="submit"
                        className="chat-send-button"
                        onClick={sendMessage}
                    >Send</button>
                </div> 
            </form>
        </div>
    )
}

const Message = ({name, message}) => {
    return (
        <div className="single-message">
            <span className="message-username">{name}:</span>
            <span className="message-msg">{message}</span>
        </div>
    )
}

export default Chat