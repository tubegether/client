import "../css/LoadingCircle.css"

const LoadingCircle = ({size}) => {
    return <div className="loading-crircle" style={{width: size, height: size}}/>
}

export default LoadingCircle