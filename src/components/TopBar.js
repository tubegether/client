import "../css/TopBar.css"
import { useState, useRef, useEffect } from 'react'
import useClickOutside from '../hooks/useClickOutside.js'
import {ReactComponent as PlayIcon} from '../svg/play-border.svg'
import {ReactComponent as QueueIcon} from '../svg/queue.svg'
import {ReactComponent as TwitchIcon} from '../svg/twitch.svg'
import {ReactComponent as YoutubeIcon} from '../svg/youtube.svg'
import {ReactComponent as CloseIcon} from '../svg/trash.svg'
import axios from 'axios'
import getYoutubeID from 'get-youtube-id'
import GOOGLE_API from '../api/googleapi'
import LoadingCircle from "./LoadingCircle"
import TWITCH_API from "../api/twitchapi"
import useWindowSize from "../hooks/useWindowSize"

const DEFAULT_URLS = {
    youtube: 'https://www.youtube.com/watch?v=',
    twitch: 'https://www.twitch.tv/'
}

const TopBar = ({searchString, setSearchString, setPlaylist, setNowPlaying, socket, roomID, setMobileSearchOpen}) => {
    const [dropdownOpen, setDropdownOpen] = useState(false)
    const [videoData, setVideoData] = useState(null)
    const [loading, setLoading] = useState(false)
    const topBarSearchRef = useRef()
    const windowSize = useWindowSize()

    const handleDropdown = () => setDropdownOpen(true)

    useClickOutside(topBarSearchRef, () => {
        if(windowSize.width >= 768) {
            setDropdownOpen(false)
            //setSearchString("")
            //setVideoData(null)
        }
    })

    const getVideoData = async () => {
        const wb = whatIsThisWebsite(searchString)

        if(wb === 'youtube') {
            const videoID = getYoutubeID(searchString)

            if(videoID != null) {
                try {
                    const info = await axios.get(`https://www.googleapis.com/youtube/v3/videos?id=${videoID}&part=snippet%2CcontentDetails%2Cstatistics&key=${GOOGLE_API}`)

                    const item = info.data.items[0]
                    
                    if(info.data.items.length != 0) {
                        setVideoData([
                            {
                                url: DEFAULT_URLS.youtube + (item?.id?.videoId || item?.id),
                                thumbnail:  item?.snippet?.thumbnails?.maxres?.url || 
                                            item?.snippet?.thumbnails?.standard?.url || 
                                            item?.snippet?.thumbnails?.default?.url,
                                title: item?.snippet?.title,
                                channel: item?.snippet?.channelTitle,
                                website: wb,
                                type: item?.snippet?.liveBroadcastContent === "live" ? "live" : "video"
                            }
                        ])
                    } else {
                        setVideoData(null)
                    }
                } catch(e) {
                    setLoading(false)
                    setVideoData(null)
                }
            } else {
                if(searchString != "") {
                    try {
                        const searchResult = await axios.get(`https://youtube.googleapis.com/youtube/v3/search?part=snippet&type=video&maxResults=10&q=${searchString}&key=${GOOGLE_API}`)
    
                        if(searchResult.data.items.length != 0) {
                            setVideoData(searchResult.data.items.map((item, index) => {
                                return {
                                    url: DEFAULT_URLS.youtube + (item?.id?.videoId || item?.id),
                                    thumbnail:  item?.snippet?.thumbnails?.maxres?.url || 
                                                item?.snippet?.thumbnails?.standard?.url || 
                                                item?.snippet?.thumbnails?.default?.url,
                                    title: item?.snippet?.title,
                                    channel: item?.snippet?.channelTitle,
                                    website: wb,
                                    type: item?.snippet?.liveBroadcastContent === "live" ? "live" : "video" 
                                }
                            }))
                        } else {
                            setVideoData(null)
                        }
                    } catch(e) {
                        setLoading(false)
                        setVideoData(null)
                    }
                }
            }
        }

        if(wb === "twitch") {
            try {
                const twitchUser = new URL(searchString).pathname.replace("/", "")
                let info
                if(twitchUser.includes("videos")) {
                    const videoId = twitchUser.replace("videos/", "")
                    info = await TWITCH_API.get(`https://api.twitch.tv/helix/videos?id=${videoId}`)
                } else {
                    info = await TWITCH_API.get(`https://api.twitch.tv/helix/streams?user_login=${twitchUser}`)
                }
                
                const item = info.data.data[0]

                setVideoData([
                    {
                        url: DEFAULT_URLS.twitch + twitchUser,
                        thumbnail: item?.thumbnail_url.
                                        replace("{width}", "120").
                                        replace("{height}", "70").
                                        replace("%120", "120").
                                        replace("%70", "70"),
                        title: item?.title,
                        channel: item?.user_name || item?.broadcaster_name,
                        website: wb,
                        type: item?.type === 'live' ? 'live' : 'video'  
                    }
                ])

                //setVideoData(info.data.data)
            } catch(e) {
                setLoading(false)
                setVideoData(null)
            }
        }

        setLoading(false)
    }


    const handlePlayNow = (url, website, type) => {
        setNowPlaying({url: url, website: website, type: type})
        setDropdownOpen(false)
        setSearchString("")
        setVideoData(null)
        sessionStorage.setItem("now-playing", JSON.stringify({url: url, website: website, type: type}))
        socket.emit("play-now", {url: url, website: website, type: type, room: roomID})

        if(windowSize.width < 768) {
            setMobileSearchOpen(false)
        }
    }

    const handleAddToQueue = (videoData) => {
        socket.emit("add-to-queue", {link: (videoData?.url), data: videoData, room: roomID})
        setPlaylist(prev => {
            sessionStorage.setItem("playlist", JSON.stringify([...prev, {link: (videoData?.url), data: videoData}]))
            return [...prev, {link: (videoData?.url), data: videoData}]
        })

        setDropdownOpen(false)
        setSearchString("")
        setVideoData(null)

        if(windowSize.width < 768) {
            setMobileSearchOpen(false)
        }
    }

    useEffect(() => {
        setVideoData(null)

        if(searchString != "") {
            setLoading(true)
        }

        const searchTimeout = setTimeout(() => {
            if(searchString != "")
                getVideoData()
        }, 600)
        return () => clearTimeout(searchTimeout)
    }, [searchString])


    return (
        <>
        <div className="top-bar">
            <div className="logo">
                <span className="thm">TUBE</span>
                <span>GETHER</span>
            </div>
            <div className="top-bar-search" ref={topBarSearchRef}>
                <input 
                    type="text" 
                    className={`top-bar-input ${dropdownOpen && 'top-bar-input-click'}`} 
                    placeholder="Search or paste link to media"
                    value={searchString}
                    onChange={e => setSearchString(e.target.value)}
                    onClick={handleDropdown}/>
                {
                    (loading && windowSize.width >= 768 && searchString != "") && 
                        <div className="loading-container">
                            <LoadingCircle size="30px" />
                        </div>
                }
                {
                    (searchString != "" && dropdownOpen && videoData && windowSize.width >= 768) &&
                    <div className="search-result-container">
                        {
                            videoData.map((item, index) => {
                                return (
                                    <SearchResult
                                        key={index} 
                                        videoData={item} 
                                        handleAddToQueue={handleAddToQueue}
                                        handlePlayNow={handlePlayNow} />
                                )
                            }) 
                        }
                    </div>
                }
            </div>
            {
                windowSize.width < 768 && 
                    <div 
                     className="close-mobile-search"
                     onClick={() => setMobileSearchOpen(false)}>
                        <CloseIcon className="svg-icon close-mobile-search-icon" />
                    </div>
            }
        </div>
        {
                    (loading && windowSize.width < 768) && 
                        <div className="mobile-loading-container">
                            <LoadingCircle size="30px" />
                        </div>
                }
        {
            windowSize.width < 768 && 
            <div className="mobile-search-results">
                {
                    (searchString != "" && videoData) &&
                    <div className="mobile-search-results-container">
                        {
                            videoData.map((item, index) => {
                                return (
                                    <SearchResult
                                        key={index} 
                                        videoData={item} 
                                        handleAddToQueue={handleAddToQueue}
                                        handlePlayNow={handlePlayNow} />
                                )
                            }) 
                        }
                    </div>
                }
            </div>
        }
        </>
    )
}

const SearchResult = ({videoData, handlePlayNow, handleAddToQueue}) => {
    const [imageLoaded, setImageLoaded] = useState(false)
    return (
        <div className="search-result">
            <div className="search-result-thumbnail">
                <div className="video-website-identifier">
                    {
                        videoData?.website === 'youtube' ? 
                            <YoutubeIcon className="svg-icon youtube-identifier" /> :
                            <TwitchIcon className="svg-icon twitch-identifier" /> 
                    }
                </div>
                {
                    !imageLoaded &&
                        <div className="image-loading">
                            <LoadingCircle size="20px" style=""/>
                        </div>
                }
                <img onLoad={() => setImageLoaded(true)} src={videoData.thumbnail} width="120px"/>
            </div>
            <div className="search-result-details">
                <h2 
                    className="search-result-title"
                    title={videoData?.title}
                    >{videoData?.title}</h2>
                <div className="search-result-author-type">
                    <h4 className="search-result-author">{videoData?.channel}</h4>
                    {
                        videoData?.type === "live" && 
                        <div className="its-live live-search-result">
                            <span>LIVE</span>
                        </div>
                    }
                </div>
                <div className="search-result-buttons">
                    <div 
                        className="play-search" 
                        type="button"
                        title="Play now"
                        onClick={ () => handlePlayNow(videoData?.url, videoData?.website, videoData?.type)}>
                        <PlayIcon className="svg-icon play-search-icon"/>
                        Play
                    </div>
                    <div 
                        className="to-queue" 
                        type="button"
                        title="Add to queue"
                        onClick={ () => handleAddToQueue(videoData) } >
                        <QueueIcon className="svg-icon to-queue-icon"/>
                        Queue
                    </div>
                </div>
            </div>
        </div>
    )
}

const whatIsThisWebsite = (url) => {
    let wb

    try {
        wb = new URL(url).hostname
        wb = wb.replace('www.', '')
        wb = wb.replace('.com', '')
        wb = wb.replace('.tv', '')
    } catch(error) {
        wb = "youtube"
    }


    return wb
}

export default TopBar