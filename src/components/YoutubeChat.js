import getYoutubeID from 'get-youtube-id'

const STYLES = {
    width: "100%",
    height: "100%"
}

const YoutubeChat = ({url}) => {
    const id = getYoutubeID(url)

    return (
        <div className="youtube-chat" style={STYLES}>
            <iframe id="youtube-chat-embed"
                    src={`https://www.youtube.com/live_chat?v=${id}&embed_domain=tubegether.onrender.com`}
                    height="100%"
                    width="100%"
                    style={{border: "none"}}>
            </iframe>
        </div>
    )
}

export default YoutubeChat
