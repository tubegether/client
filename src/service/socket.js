import ENDPOINT from "../endpoint";
import io from "socket.io-client"

export const socket = io(ENDPOINT)